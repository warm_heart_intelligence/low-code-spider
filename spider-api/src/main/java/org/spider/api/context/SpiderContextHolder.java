package org.spider.api.context;

import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * 使用threadlocal，那每开一个task，就有一个线程，就会有一份Context上下文副本
 * 这样就不用手动创建一个字典，来存储和管理不同任务的上下文信息了。<br>
 * 程序设计就只需要专注于完成一个任务的设计了。因为上下文环境实现了线程隔离，不同线程有不同的上下文
 */
public class SpiderContextHolder {
    private static final ThreadLocal<SpiderContext> THREAD_LOCAL = new TransmittableThreadLocal<>();

    public static SpiderContext get(){
        return THREAD_LOCAL.get();
    }

    public static void set(SpiderContext context) {
        THREAD_LOCAL.set(context);
    }

    public static void remove() {
        THREAD_LOCAL.remove();
    }
}
