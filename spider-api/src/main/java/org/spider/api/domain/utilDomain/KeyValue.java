package org.spider.api.domain.utilDomain;

import lombok.Data;
@Data
public class KeyValue {
    String key,value;
    public KeyValue(String key,String value){
        this.key=key;
        this.value=value;
    }
}
