package org.spider.api.domain.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName notice
 */
@TableName(value ="notice")
@Data
public class Notice implements Serializable {
    private String id;

    private String email;

    private Integer start;

    private Integer exception;

    private Integer end;

    private static final long serialVersionUID = 1L;
    public Notice(){}
    public Notice(String id) {this.id=id;}

    public boolean checkStart(){
        return "1".equalsIgnoreCase(String.valueOf(start));
    }
    public boolean checkException(){
        return "1".equalsIgnoreCase(String.valueOf(exception));
    }
    public boolean checkEnd(){
        return "1".equalsIgnoreCase(String.valueOf(end));
    }
}