package org.spider.api.domain.vo;

import lombok.Data;

@Data
public class DatasourceVo {
    private Integer id;

    private String name;
}
