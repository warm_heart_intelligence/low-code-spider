package org.spider.api.domain.vo;

import lombok.Data;
import org.spider.api.domain.model.SpiderFlow;
@Data
public class SpiderFlowVo extends SpiderFlow {
    private String runningAll;

    @Override
    public String toString() {
        return " runningAll="+ runningAll +" "+super.toString() ;
    }
}
