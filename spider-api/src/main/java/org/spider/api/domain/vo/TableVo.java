package org.spider.api.domain.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TableVo {
    private String tableName;
    private List<Map<String,String>> fields;
}
