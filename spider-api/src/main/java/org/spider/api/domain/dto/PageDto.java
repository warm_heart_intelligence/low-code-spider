package org.spider.api.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class PageDto {
    Integer total,size;
    Integer page,limit;
    List<?> records;
}
