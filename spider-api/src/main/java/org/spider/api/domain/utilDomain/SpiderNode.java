package org.spider.api.domain.utilDomain;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 一种图的数据结构，一个节点有多个前驱，多个后继。
 * 节点有特定的属性以及自己的属性
 */
@Data
public class SpiderNode {
    // 节点属性
    /**
     * 节点名称
     */
    private String nodeName;
    /**
     * 节点ID
     */
    private String nodeId;
    private String shape;
    /**
     * 节点的Json属性
     */
    private Map<String, Object> jsonProperty = new HashMap<>();
    // 连线关系
    /**
     * 节点列表中的下一个节点
     */
    private List<SpiderNode> nextNodes = new ArrayList<>();
    // 工作流
    /**
     * 传递变量
     */
    private Map<String, String> condition = new HashMap<>();
    /**
     * 异常流转
     */
    private Map<String, String> exception = new HashMap<>();

    // 获取属性值
    public String getStringByKey(String key) {
        if (jsonProperty == null) return null;
        Object r = this.jsonProperty.get(key);
        return r!=null? r.toString() : "";
    }

    public String getStringByKey(String key, String defaultValue) {
        String value = getStringByKey(key);
        return StringUtils.isNotBlank(value) ? value : defaultValue;
    }


    public List<KeyValue> getDictListByKey(String key){
        if (jsonProperty == null) return null;
        List<KeyValue> result = new ArrayList<>();
        JSONArray variableArray = (JSONArray) jsonProperty.get(key);
        if (variableArray == null) return null;
        for (int i = 0; i < variableArray.size(); i++) {
            JSONObject item = variableArray.getJSONObject(i);
            String name = item.getString("name");
            String value = item.getString("value");
            result.add(new KeyValue(name,value));
        }
        return result;
    }

    public Map<String, String> getMapByKey(String key) {
        if (jsonProperty == null) return null;
        Map<String, String> result = new HashMap<>();
        JSONArray variableArray = (JSONArray) jsonProperty.get(key);
        if (variableArray == null) return null;
        for (int i = 0; i < variableArray.size(); i++) {
            JSONObject item = variableArray.getJSONObject(i);
            String name = item.getString("name");
            String value = item.getString("value");
            result.put(name, value);
        }
        return result;
    }

    public String getCondition(String fromNodeId) {
        return condition.get(fromNodeId);
    }

//    public String getExceptionFlow(String fromNodeId) {
//        return exception.get(fromNodeId);
//    }


    //这个得override，因为图会存在环路，那么nextNodes和prevNodes的打印是会导致死循环
    @Override
    public String toString() {
        return "SpiderNode{" +
                "nodeName='" + nodeName + '\'' +
                ", nodeId='" + nodeId + '\'' +
                ", jsonProperty=" + jsonProperty +
                ", conditions=" + condition +
                '}';
    }
}