package org.spider.api.domain.vo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FileVo {
    private String filename;
    private String createTime;
    private Long length;
    private String path;
}
