package org.spider.api.domain.vo;

import lombok.Data;
import org.spider.api.domain.model.Task;
@Data
public class TaskVo extends Task {
    private String timeElapsed; //使用的时间

    @Override
    public String toString() {
        return "TaskDto{" +
                "id=" + getId() +
                ", flowId='" + getFlowId() + '\'' +
                ", beginTime=" + getBeginTime() +
                ", endTime=" + getEndTime() +
                ", name='" + getName() + '\'' +
                ", timeElapsed='"+ getTimeElapsed() + "\'" +
                '}';
    }
}
