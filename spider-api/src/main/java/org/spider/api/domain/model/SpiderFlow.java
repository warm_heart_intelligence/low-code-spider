package org.spider.api.domain.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.spider.api.enums.CronEnableType;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName flow
 */
@TableName(value ="flow")
@Data
public class SpiderFlow implements Serializable {
    /**
     * 
     */
    @TableId
    private String id;
    private String name;
    private String comment;

    private Integer userId;

    private String config;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastExecuteTime;

    private String cron;
    private CronEnableType cronEnable;

    @TableField(exist = false)
    private static final long serialVersionUID = 34872394L;
}