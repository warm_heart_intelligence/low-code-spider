package org.spider.api.domain.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName ip_proxy
 */
@TableName(value ="ip_proxy")
@Data
public class IpProxy implements Serializable {
    private Integer id;

    private String type;

    private String url;

    private static final long serialVersionUID = 1L;
}