package org.spider.api.domain.utilDomain;

import lombok.Data;

@Data
public class Result {
    private Integer code = 1;

    private String message = "执行成功";

    private Object data;

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    // 不传递数据，只传递消息
    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(Object data) {
        this(1, null, data);
    }

    public static Result getInstance(Object data) {
        return new Result(data);
    }

    public static Result getInstance(Integer code, String message, Object data) {
        return new Result(code, message, data);
    }

    public static Result getInstance(Integer code, String message) {
        return new Result(code, message);
    }

    public static Result getInstance(boolean flag) {
        if (flag) return new Result(1, "操作成功");
        else return new Result(0, "操作失败");
    }
}
