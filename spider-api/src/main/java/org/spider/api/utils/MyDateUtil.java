package org.spider.api.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDateUtil {

    public static final String DATE_TIME_PATTERN =
            "yyyy:MM:dd HH:mm:ss";
    public static final SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_PATTERN);

    public static String nowString() {
        return sdf.format(new Date());
    }

    public static String format(Date date){
        return sdf.format(date);
    }
}
