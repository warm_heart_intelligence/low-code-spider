package org.spider.api.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum CronEnableType {
    on("true","启用定时任务"),
    off("false","关闭定时任务"),;
    @EnumValue
    final String value;
    final String desc;

    CronEnableType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
