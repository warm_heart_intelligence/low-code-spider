package org.spider.api.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

public enum NoticeType {
    start,
    exception,
    end

//    start("start",""),
//    exception("exception",""),
//    end("end","")
//    ;
//    @EnumValue
//    String status;
//
//    String desc;
//
//    NoticeType(String status, String desc) {
//        this.status = status;
//        this.desc = desc;
//    }
}
