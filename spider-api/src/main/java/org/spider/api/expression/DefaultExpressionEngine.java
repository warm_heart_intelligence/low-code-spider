package org.spider.api.expression;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spider.api.executor.FunctionExecutor;
import org.spider.api.executor.FunctionExtension;
import org.spider.api.expression.interfaces.ExpressionEngine;
import org.spider.api.expression.interpreter.Reflection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Component
public class DefaultExpressionEngine implements ExpressionEngine {
	
	@Autowired
	private List<FunctionExecutor> functionExecutors;

	@Autowired
	private List<FunctionExtension> functionExtensions;
	private static Logger logger = LoggerFactory.getLogger(DefaultExpressionEngine.class);
	
	@PostConstruct
	private void init(){
		for (FunctionExtension extension : functionExtensions) {
			Reflection.getInstance().registerExtensionClass(extension.support(), extension.getClass());
		}
	}
	
	@Override
	public Object execute(String expression, Map<String, Object> variables) {
		if(StringUtils.isBlank(expression)){
			return expression;
		}
		logger.debug("开始解析表达式:"+expression);
		ExpressionTemplateContext context = new ExpressionTemplateContext(variables);
		for (FunctionExecutor executor : functionExecutors) {
			context.set(executor.getFunctionPrefix(), executor);
		}
		ExpressionGlobalVariables.getVariables().entrySet().forEach(entry->{
			context.set(entry.getKey(),
					ExpressionTemplate.create(entry.getValue()).render(context)
			);
		});
		try {
			ExpressionTemplateContext.set(context);
			return ExpressionTemplate.create(expression).render(context);
		} finally {
			ExpressionTemplateContext.remove();
		}
	}
	
}
