package org.spider.api.expression.interfaces;

import java.util.List;

/**
 *  AST树使用
 */
public interface DynamicMethod {
	
	Object execute(String methodName, List<Object> parameters);

}
