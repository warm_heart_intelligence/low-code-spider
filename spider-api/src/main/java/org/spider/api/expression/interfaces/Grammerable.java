package org.spider.api.expression.interfaces;


import org.spider.api.domain.utilDomain.Grammer;

import java.util.List;

public interface Grammerable {

	List<Grammer> grammers();
	
}
