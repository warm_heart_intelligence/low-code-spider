package org.spider.api.concurrent;

import org.spider.api.domain.utilDomain.SpiderNode;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SeqStrategyQueue implements StrategyQueue{
    private List<SpiderFutureTask<?>> list=new CopyOnWriteArrayList<>();
    @Override
    public Comparator<SpiderNode> comparator() {
        // 顺序 从前往后提取
        return (o1,o2)-> -1;
    }

    @Override
    public void add(SpiderFutureTask<?> task) {
        list.add(task);
    }

    @Override
    public SpiderFutureTask<?> get() {
        return list.remove(0);
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }
}
