package org.spider.api.concurrent;

import org.spider.api.domain.utilDomain.SpiderNode;

import java.util.Comparator;

public interface StrategyQueue {
    // 定义比较规则
    Comparator<SpiderNode> comparator();
    // 入队
    void add(SpiderFutureTask<?> task);
    // 出队
    SpiderFutureTask<?> get();
    // 判空
    boolean isEmpty();
}
