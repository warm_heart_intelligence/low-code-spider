package org.spider.api.concurrent;

import org.spider.api.domain.utilDomain.SpiderNode;

import java.util.concurrent.FutureTask;
//继承FutureTask,因为要用node来进行比较
public class SpiderFutureTask<T> extends FutureTask {
    private SpiderNode node;

    public SpiderFutureTask(Runnable runnable, T result, SpiderNode node){
        super(runnable,result);//result是Future的get()方法的返回值
        this.node=node;
    }

    public SpiderNode getNode() {
        return node;
    }
}
