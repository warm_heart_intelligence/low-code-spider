package org.spider.api.listener;

import org.spider.api.context.SpiderContext;

public interface TaskListener {
    void beforeListener();
    void afterListener();
}
