package org.spider.api.executor;

/**
 * FunctionExecutor接口的实现类提供了对应的函数操作
 */
public interface FunctionExecutor {
	String getFunctionPrefix();
}
