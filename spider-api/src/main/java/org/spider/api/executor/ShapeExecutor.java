package org.spider.api.executor;

import org.spider.api.context.SpiderContext;
import org.spider.api.domain.utilDomain.SpiderNode;

import java.util.Map;
/**
 * 执行器接口<br>
 * 执行器实例各只有一份,方法是共享的,这样避免创建过多实例
 */
public interface ShapeExecutor {
	String LOOP_VARIABLE_NAME = "loopVariableName";
	
	String LOOP_COUNT = "loopCount";
	/**
	 * 节点形状
	 * @return 节点形状名称
	 */
	String supportShape();
	
	/**
	 * 执行器具体的功能实现
	 * @param node 当前要执行的爬虫节点
	 * @param context 爬虫上下文
	 * @param variables 节点流程的全部变量的集合
	 */
	void execute(SpiderNode node, SpiderContext context, Map<String, Object> variables);
	
	default boolean isThread(){
		return true;
	}
}
