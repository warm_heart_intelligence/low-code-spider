package org.spider.api.executor;

public interface FunctionExtension {
	Class<?> support();
}
