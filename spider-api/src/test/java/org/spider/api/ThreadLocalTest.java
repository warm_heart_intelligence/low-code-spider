package org.spider.api;

import com.alibaba.ttl.TransmittableThreadLocal;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.spider.api.concurrent.SeqStrategyQueue;
import org.spider.api.concurrent.SpiderThreadPoolExecutorManager;
import org.spider.api.concurrent.SpiderThreadPoolExecutorManager.SubThreadPool;

/**
 * thread
 */
@Slf4j
public class ThreadLocalTest {

    public static void main(String[] args) throws InterruptedException {
        Executor executor = new Executor();
        SpiderThreadPoolExecutorManager manager=Executor.manager;
        for (int i = 0; i < 3; i++) {
            String id="task--"+(i+1);
            Runnable task = () -> {
                executor.run(id);
            };
            log.info("main t...");
            manager.submit(task);
        }
    }
}
/*
能够正确获取传递的ThreadLocal变量
19:11:56.841 [flow-5] INFO org.spider.api.Executor - task--3
19:11:56.841 [flow-6] INFO org.spider.api.Executor - task--1
19:11:56.841 [flow-4] INFO org.spider.api.Executor - task--2
 */
@Data
@Slf4j
class Executor {
    public static SpiderThreadPoolExecutorManager manager = new SpiderThreadPoolExecutorManager(10);;

    public void run(String id) {
        SubThreadPool poolExecutor = manager.createSubThreadExecutor(10, new SeqStrategyQueue());
        Context context = new Context(poolExecutor, id);
        ContextHolder.set(context);
        try {
            //模拟任务延时
//                Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Runnable r=()->{
            Context c = ContextHolder.get();
            log.info(c.getName());
        };
        poolExecutor.submitAsync(r, null, null);
        poolExecutor.awaitTermination();
    }
}

@Data
class Context {
    SubThreadPool poolExecutor;
    private String name;

    public Context(SubThreadPool poolExecutor, String name) {
        this.poolExecutor = poolExecutor;
        this.name = name;
    }
}

@Data
class ContextHolder {
    private static final ThreadLocal<Context> threadLocal = new TransmittableThreadLocal<>();

    public static void set(Context context) {
        threadLocal.set(context);
    }

    public static Context get() {
        return threadLocal.get();
    }

    public static void remove(){
        threadLocal.remove();
    }
}
