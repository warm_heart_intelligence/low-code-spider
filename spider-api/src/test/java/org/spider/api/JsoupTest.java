package org.spider.api;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;

@Slf4j
public class JsoupTest {
    /**
     * 测出了一个运行控制的bug，关于end()方法
     * @param args
     * @throws Exception
     */
//    public static void main(String[] args) throws Exception{
//        File f = new File("F:\\Code\\FinalDesign\\low-code-spider\\data\\flow-01\\logs\\人道大圣.html");
//        System.out.println(f.exists());
//        BufferedReader br = new BufferedReader(new FileReader(f));
//        StringBuilder sbd = new StringBuilder();
//        String line ;
//        while ((line = br.readLine()) != null) {
//            sbd.append(line);
//        }
//        String html = sbd.toString();
//        Document document = Jsoup.parse(html);
//        Element listmain = document.getElementsByClass("listmain").get(0);
////        System.out.println(listmain);
//        Elements as = listmain.getElementsByTag("a");
//        //准备写入文件
//        File urls = new File(f.getParent(), "urls.txt");
//        FileWriter writer = new FileWriter(urls);
//        for (Element a : as) {
//            String url = "https://www.biqg.cc" + a.attr("href");
//            if (!(a.attr("href").contains("java"))) {
////                System.out.println(url + ",   " + a.text());
//                writer.write(url + "\n");
//            }
//        }
//        writer.close();
//        //模拟批量下载html
//        //开线程
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        RequestScheduler scheduler = new RequestScheduler(500, false, false);
//        scheduler.start();
//        List<Future<HttpResponse>> futures = new ArrayList<>();
//        for (int i = 0; i < as.size(); i++) {
//            int finalI = i;
//            Callable<HttpResponse> c = () -> {
//                Element a = as.get(finalI);
//                String url = "https://www.biqg.cc" + a.attr("href");
//                String chapter = a.text();
//                log.info("开始抓取{}, 章节:{}", url, chapter);
//                HttpResponse httpResponse = scheduler.get(new HttpRequest(url, 10));
//                String body = httpResponse.getResponse().body();
//                String s = chapterParser(body);
////                System.out.println(s);
//                OutputStream os = org.spider.api.utils.io.FileUtils.createLog("data", "fictions" + File.separator+"人道大圣",chapter , "txt");
//                os.write(s.getBytes());
//                os.close();
//                log.info("解析并写入文件");
//                log.info("statusCode:{}, 抓取{}完成, 章节:{}",httpResponse.getStatusCode(), url, chapter);
//                return httpResponse;
//            };
//            Future<HttpResponse> future = executorService.submit(c);
//            futures.add(future);
//        }
//        futures.forEach(future -> {
//            try {
//                HttpResponse httpResponse = future.get();
//                log.info("future.get()  {}",httpResponse.getUrl());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        executorService.shutdown();
//        scheduler.end();
//    }
    @Test
    public void jsoupGetElements() throws Exception {

    }

    private static String chapterParser(String chapter){
        String result = "";
        Document document = Jsoup.parse(chapter);
        Element chaptercontent = document.getElementById("chaptercontent");
        result = chaptercontent.text();
        return result;
    }
}
