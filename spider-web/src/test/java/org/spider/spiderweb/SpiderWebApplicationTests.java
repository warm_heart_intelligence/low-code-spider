package org.spider.spiderweb;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spider.api.domain.model.DataSourceModel;
import org.spider.api.domain.model.SpiderFlow;
import org.spider.api.domain.utilDomain.SpiderNode;
import org.spider.api.utils.flow.SpiderFlowUtils;
import org.spider.core.service.DatasourceService;
import org.spider.core.service.SpiderFlowService;
import org.spider.core.service.TaskService;
import org.spider.core.utils.DataSourceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SpiderWebApplicationTests {
//    @Autowired
//    UserService userService;
    @Autowired
    SpiderFlowService flowService;
    @Autowired
    TaskService taskService;
    @Autowired
    DatasourceService datasourceService;

    public static void main(String[] args) {
        System.out.println("raw json length: " + json.length());
        String simpleJson = SpiderFlowUtils.simpleJson(json);
        System.out.println("simple json length: " + simpleJson.length());
        JSONObject object = JSON.parseObject(simpleJson);
        System.out.println(object);
    }

    static String json;

    static {
        json = "{\"comments\":\"type属性暂时不用,多余了\",\"data\":{\"lineList\":[{\"condition\":\"\",\"geomProperty\":{\"common\":{}},\"source\":\"0\",\"target\":\"1\"},{\"condition\":\"\",\"geomProperty\":{\"common\":{}},\"source\":\"1\",\"target\":\"2\"},{\"condition\":\"\",\"geomProperty\":{\"common\":{}},\"source\":\"2\",\"target\":\"3\"}],\"nodeList\":[{\"parent\":\"1\",\"name\":\"开始\",\"style\":\"start\",\"id\":\"0\",\"type\":\"node\"},{\"geomProperty\":{\"x\":20,\"y\":20},\"name\":\"完美世界爬虫\",\"style\":\"request\",\"id\":\"1\",\"type\":\"node\",\"config\":{\"tls-validate\":\"1\",\"method\":\"GET\",\"shape\":\"request\",\"response-charset\":\"\",\"request-body\":\"\",\"retryCount\":\"\",\"body-type\":\"none\",\"timeout\":\"\",\"url\":\"http://127.0.0.1:8050\",\"repeat-enable\":\"0\",\"sleep\":\"3000\",\"proxy\":\"\",\"loopVariableName\":\"i\",\"loopCount\":\"10\",\"body-content-type\":\"text/plain\",\"follow-redirect\":\"1\",\"retryInterval\":\"\",\"value\":\"开始抓取\",\"cookie-auto-set\":\"1\"}},{\"geomProperty\":{\"x\":200,\"y\":200},\"name\":\"定义变量\",\"style\":\"variable\",\"id\":\"2\",\"type\":\"node\",\"config\":{\"var-value\":[\"${resp.html}\",\"${json.parse(jsonC)}\"],\"loopCount\":\"expression\",\"loopName\":\"name-1\",\"var-name\":[\"jsonC\",\"content\"]}},{\"parent\":\"1\",\"vertex\":\"1\",\"geomProperty\":{\"x\":\"410\",\"width\":\"32\",\"y\":\"70\",\"height\":\"32\"},\"style\":\"output\",\"id\":\"3\",\"value\":\"输出\",\"config\":{\"shape\":\"output\",\"csvEncoding\":\"GBK\",\"tableName\":\"\",\"output-name\":[\"内容\"],\"loopVariableName\":\"\",\"output-value\":[\"${content.data}\"],\"output-database\":\"0\",\"loopCount\":\"\",\"output-all\":\"0\",\"datasourceId\":\"1\",\"csvName\":\"\",\"output-csv\":\"0\",\"value\":\"输出\"}}]},\"name\":\"完美世界\"}";
    }

    @Test
    void contextLoads() {
        System.out.println("hello world");
        Logger log = LoggerFactory.getLogger(SpiderWebApplicationTests.class);
        log.info("hello ");
//        userService.list();
//        Flow flow=new Flow();
//        flow.setFlowId("0001");
//        System.out.println(flowService.removeById(flow.getFlowId()));
//        flow.setConfig(json);
//        flow.setName("demo1");
//        flow.setCreateDate(new Date());
//        flow.setExecuteCnt(1);
//        System.out.println(flowService.save(flow));
        int cnt = 0;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < json.length(); i++) {
            char ch = json.charAt(i);
            if (ch != ' ' && ch != '\n') {
                cnt++;
                sb.append(ch);
            }
        }
        System.out.println(json);
        System.out.println(sb);
        System.out.println(sb.length());
        System.out.println(json.length());
        System.out.println(cnt);

        SpiderNode spiderNode = SpiderFlowUtils.loadJsonFromString(sb.toString());
        System.out.println(spiderNode);
    }

    @Test
    void getLength() {
        SpiderFlow byId = flowService.getById("0001");
        String config = byId.getConfig();
        String trimmed = config.trim();
        System.out.println(trimmed.length());
        System.out.println(byId.getConfig().length());
    }

    @Test
    void updateConfig() {
        SpiderFlow byId = flowService.getById("0001");
        byId.setConfig(SpiderFlowUtils.simpleJson(byId.getConfig()));
        System.out.println(flowService.updateById(byId));
    }

    @Test
    void test_dsService() {
        DataSourceModel old = datasourceService.getById(1);
        if (old == null) {
            old = new DataSourceModel();
            old.setDriverClassName("com.mysql.cj.jdbc.Driver");
            old.setUrl("jdbc:mysql://localhost:3306/low_code_spider?useSSL=false&useUnicode=true&characterEncoding=UTF8&autoReconnect=true");
            old.setUsername("root");
            old.setPassword("123456");
            boolean saved = datasourceService.save(old);
            System.out.println(saved);
        }
        System.out.println(old);
        DataSource dataSource = DataSourceUtils.createDataSource(old.getDriverClassName(),
                old.getUrl(), old.getUsername(), old.getPassword());
        JdbcTemplate template=new JdbcTemplate(dataSource);
        String sql = "select * from flow";
        Map<String, Object> stringObjectMap = template.queryForMap(sql);
        System.out.println(stringObjectMap);
        SqlRowSet sqlRowSet = template.queryForRowSet(sql);
        System.out.println(sqlRowSet);
    }
}
