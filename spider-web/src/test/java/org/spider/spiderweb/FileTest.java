package org.spider.spiderweb;

import org.junit.jupiter.api.Test;
import org.spider.api.domain.vo.FileVo;
import org.spider.core.service.FileService;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FileTest {

    @Resource
    private FileService service;

    @Test
    public void  t(){
        List<FileVo> list = service.list();
        list.forEach(System.out::println);
    }

    public long sumDigitDifferences(int[] nums) {
        long ans = 0;
        int[][] cnt = new int[Integer.toString(nums[0]) . length()][10];
        for (int k = 0; k < nums.length; k++) {
            int x = nums[k];
            for (int i = 0; x > 0; x /= 10, i++) {
                int d = x % 10;
                ans += k - cnt[i][d]++; // 前面有这么多个不同于 d 的数
            }
        }
        return ans;
    }

}
