package org.spider.spiderweb;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.spider.api.domain.model.SpiderFlow;
import org.spider.api.domain.vo.SpiderFlowVo;
import org.spider.core.service.SpiderFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpiderFlowJson {
    @Autowired
    SpiderFlowService flowService;

    @Test
    void printJson(){
        SpiderFlow flow = flowService.selectById("88baffe69047420fa8722417e6781b11");
        System.out.println(flow.getConfig());
    }
    static String yinhao = "&#39;";
    public static void main(String[] args) {
//        String json="{\"nodeList\":[{\"style\":\"start\",\"id\":0,\"geomProperty\":{\"x\":50,\"y\":50},\"config\":{}},{\"style\":\"request\",\"id\":1,\"geomProperty\":{\"x\":306,\"y\":190},\"config\":{\"url\":\"https://www.biqg.cc/book/4070/2.html\"}},{\"style\":\"variable\",\"id\":2,\"geomProperty\":{\"x\":514,\"y\":226},\"config\":{\"variable\":[{\"name\":\"chapter\",\"value\":\"${resp.xpath('//div[@id=\\\"chaptercontent\\\"]/text()')}\"}]}},{\"style\":\"output\",\"id\":3,\"geomProperty\":{\"x\":773,\"y\":314},\"config\":{}}],\"lineList\":[{\"source\":0,\"target\":1,\"sourceType\":\"Rectangle\",\"targetType\":\"Dot\"},{\"source\":1,\"target\":2,\"sourceType\":\"Rectangle\",\"targetType\":\"Dot\"},{\"source\":2,\"target\":3,\"sourceType\":\"Rectangle\",\"targetType\":\"Dot\"}]}";
//        json = json.replace("'",yinhao);
//        JSONObject jsonObject = JSON.parseObject(json);
//        SpiderNode spiderNode = SpiderFlowUtils.loadJsonFromString(json);
//        System.out.println(jsonObject);
//        System.out.println(spiderNode);
        String json="{\"nodeList\":[{\"style\":\"start\",\"id\":0,\"geomProperty\":{\"x\":50,\"y\":50},\"config\":{}},{\"style\":\"request\",\"id\":1,\"geomProperty\":{\"x\":306,\"y\":190},\"config\":{\"url\":\"https://www.biqg.cc/book/4070/2.html\"}},{\"style\":\"variable\",\"id\":2,\"geomProperty\":{\"x\":514,\"y\":226},\"config\":{\"variable\":[{\"name\":\"chapter\",\"value\":\"${resp.html()&#39;}\"}]}},{\"style\":\"output\",\"id\":3,\"geomProperty\":{\"x\":773,\"y\":314},\"config\":{}}],\"lineList\":[{\"source\":0,\"target\":1,\"sourceType\":\"Rectangle\",\"targetType\":\"Dot\"},{\"source\":1,\"target\":2,\"sourceType\":\"Rectangle\",\"targetType\":\"Dot\"},{\"source\":2,\"target\":3,\"sourceType\":\"Rectangle\",\"targetType\":\"Dot\"}]}";
        json = json.replace(yinhao,"'");
        System.out.println(json);
    }

    @Test
    void page(){

        IPage<SpiderFlowVo> spiderFlowVoIPage = flowService.selectPage(new Page<>(1, 10), "");
        spiderFlowVoIPage.getRecords().forEach(System.out::println);
    }


}
