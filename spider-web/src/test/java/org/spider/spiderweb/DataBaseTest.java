package org.spider.spiderweb;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.junit.jupiter.api.Test;
import org.spider.api.domain.model.HistoryConfig;
import org.spider.api.domain.model.SpiderFlow;
import org.spider.api.utils.flow.SpiderFlowUtils;
import org.spider.core.service.DatasourceService;
import org.spider.core.service.HistoryConfigService;
import org.spider.core.service.SpiderFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DataBaseTest {
    @Autowired
    private SpiderFlowService flowService;
    @Autowired
    private HistoryConfigService historyConfigService;
    @Test
    void insertFlows(){
        List<SpiderFlow> list = flowService.list();
        for (int i = 0; i < 10; i++) {
            for(SpiderFlow flow:list){
                HistoryConfig config=new HistoryConfig();
                config.setFlowId(flow.getId());
                config.setCreateTime(new Date());
                config.setConfig(flow.getConfig());
                System.out.println(historyConfigService.save(config));
            }
        }
    }

    @Test
    void listConfig(){
        LambdaQueryWrapper<HistoryConfig> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(HistoryConfig::getFlowId,"0001");
        List<HistoryConfig> list = historyConfigService.list(wrapper);
        System.out.println(list);
    }

    @Test
    void copy() throws InterruptedException {
        SpiderFlow flow=flowService.getById("0001");
        System.out.println(flow);
        for (int i = 0; i < 10; i++) {
            SpiderFlow f=new SpiderFlow();
            f.setConfig(flow.getConfig());
            f.setCreateDate(new Date());
            f.setName("demo-"+(i+1));
            f.setId(SpiderFlowUtils.getUUID());
//            f.setExecuteCnt((i+1));
            Thread.sleep(500);
            flowService.save(f);
        }
    }

    @Resource
    private DatasourceService datasourceService;
    @Test
    void tableNames(){
        String dsId = "20";
        List<String> strings = datasourceService.listTables(dsId);
        strings.forEach(System.out::println);
    }
}
