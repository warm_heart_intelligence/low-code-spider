package org.spider.spiderweb;

import org.junit.jupiter.api.Test;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RedisTest {
    @Resource
    private RedissonClient redissonClient;
    @Test
    void testTemplate(){
        System.out.println(stringRedisTemplate.opsForSet().members("20711dbf140241d99af69abae780c597"));

        RLock lock = redissonClient.getLock("20711dbf140241d99af69abae780c597");
        boolean isLock = lock.tryLock();
        if(!isLock){
            System.out.println("获取锁失败");
            return;
        }
        try {
            stringRedisTemplate.opsForSet().add("20711dbf140241d99af69abae780c597","www.baidu.com");
            System.out.println(stringRedisTemplate.opsForSet().members("20711dbf140241d99af69abae780c597"));
        }finally {
            lock.unlock();
        }
    }

    @Resource
    StringRedisTemplate stringRedisTemplate;

}
