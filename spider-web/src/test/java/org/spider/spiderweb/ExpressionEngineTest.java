package org.spider.spiderweb;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.spider.api.domain.model.SpiderFlow;
import org.spider.api.domain.utilDomain.SpiderNode;
import org.spider.api.expression.interfaces.ExpressionEngine;
import org.spider.api.utils.flow.SpiderFlowUtils;
import org.spider.core.Spider;
import org.spider.core.job.SpiderJob;
import org.spider.core.job.SpiderJobContext;
import org.spider.core.service.SpiderFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class ExpressionEngineTest {
    @Autowired
    ExpressionEngine engine;
    static Map<String, Object> variables = new HashMap<>();

    static {
        variables.put("chapter", "perfect world");
    }

    @Test
    void execute() {
        Object executed = engine.execute("${chapter}happy", variables);
        System.out.println(executed.toString());
    }

    @Test
    void element() throws Exception {
        File f = new File("F:\\Code\\FinalDesign\\low-code-spider\\data\\flow-01\\logs\\人道大圣.html");
        System.out.println(f.exists());
        BufferedReader br = new BufferedReader(new FileReader(f));
        StringBuilder sbd = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sbd.append(line);
        }
        String html = sbd.toString();
        Document document = Jsoup.parse(html);
    }

    @Autowired
    private Spider spider;
    @Autowired
    private SpiderFlowService spiderFlowService;

    @Test
    void runFlow() {
        LambdaQueryWrapper<SpiderFlow> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SpiderFlow::getId, "0001");
        SpiderFlow flow = spiderFlowService.getOne(wrapper);
//        log.info(flow.toString());
        SpiderNode root = SpiderFlowUtils.loadJsonFromString(flow.getConfig());

        log.info(root.toString());
        log.info(root.getNextNodes().toString());

        SpiderJobContext spiderJobContext = SpiderJobContext.create("data","flow-01",001,true);
        spider.run(flow, spiderJobContext, new HashMap<>());
    }
    @Autowired
    private SpiderJob job;

    @Test
    void threadPoolTest() throws ExecutionException, InterruptedException {
        Future<?> submit = Spider.executor.submit(() -> job.run("20711dbf140241d99af69abae780c597","tesst"));
        submit.get();
//        log.info("Future:{}",submit);
//        log.info(submit.get().toString());
//        job.run("0001");
    }

    @Test
    void testFlow(){
        SpiderFlow flow=spiderFlowService.selectById("88baffe69047420fa8722417e6781b11");
        SpiderJobContext spiderJobContext = SpiderJobContext.create("data","88baffe69047420fa8722417e6781b11",001,true);
        SpiderNode spiderNode = SpiderFlowUtils.loadJsonFromString(flow.getConfig());
        spider.executeRoot(spiderNode,spiderJobContext,new HashMap<>());
    }
}
