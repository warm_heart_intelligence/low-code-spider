package org.spider.spiderweb;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.spider.api.domain.vo.TaskVo;
import org.spider.api.domain.model.SpiderFlow;
import org.spider.api.domain.model.Task;
import org.spider.core.job.SpiderJob;
import org.spider.core.mapper.TaskMapper;
import org.spider.core.service.SpiderFlowService;
import org.spider.core.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class TaskTest {
    @Autowired
    TaskService taskService;
    @Autowired
    TaskMapper taskMapper;
    @Autowired
    SpiderFlowService spiderFlowService;
    @Test
    void save(){
        SpiderFlow flow = spiderFlowService.getById("20711dbf140241d99af69abae780c597");
        log.info(flow.getId());
        Task task=new Task();
        task.setFlowId(flow.getId());
        task.setBeginTime(new Date());
        taskService.save(task);
        //save后，Task的id会被自动赋值
        log.info(task.toString());
    }

    @Test void selectTask(){
        IPage<TaskVo> list= taskMapper.selectPage(new Page<>(0, 5),"20711dbf140241d99af69abae780c597", "");
        list.getRecords().forEach(System.out::println);
    }

    @Test
    void testCount(){
        String flowId="20711dbf140241d99af69abae780c597";
        Integer all=taskMapper.getAllCountByFlowId(flowId);
        Integer finished = taskMapper.getFinishedCountByFlowId(flowId);
        System.out.println((all-finished)+"/"+all);
    }

    @Resource
    SpiderJob spiderJob;
    @Test
    void runWithTest(){
        spiderJob.run("20711dbf140241d99af69abae780c597",
                "linux docker环境下测试");
    }
}
