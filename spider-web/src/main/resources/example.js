/**
 * 以这个为例子  0->1->2->3
 */
const example = {
    "nodeList": [{"style": "start", "id": 0, "geomProperty": {"x": 50, "y": 50}, "config": {}}, {
        "style": "request",
        "id": 1,
        "geomProperty": {"x": 280, "y": 50},
        "config": {
            "url": "https://www.biqg.cc/book/4070/",
            "retryCount": "2",
            "retryInterval": "6000",
            "timeout": "",
            "nodeName": "开始抓取"
        }
    }, {
        "style": "variable",
        "id": 2,
        "geomProperty": {"x": 434, "y": 49},
        "config": {"variable": [{"name": "chapters", "value": "${resp.xpaths(&#39;//dd&#39;)}"}]}
    }, {
        "style": "request",
        "id": 3,
        "geomProperty": {"x": 628, "y": 49},
        "config": {
            "url": "https://www.biqg.cc/${chapters[i+1].xpath(&#39;//a/@href&#39;)}",
            "retryCount": "3",
            "retryInterval": "5000",
            "loopVariableName": "i",
            "loopCount": "5",
            "sleep": "5000"
        }
    }, {
        "style": "variable",
        "id": 4,
        "geomProperty": {"x": 48, "y": 174},
        "config": {
            "variable": [{
                "name": "content",
                "value": "${resp.xpath(&#39;//div[@id=\"chaptercontent\"]/text()&#39;)}"
            }]
        }
    }, {
        "style": "output",
        "id": 5,
        "geomProperty": {"x": 283, "y": 186},
        "config": {
            "output": [{
                "name": "chapter_title",
                "value": "${chapters[i+1].xpath(&#39;//a/text()&#39;)}"
            }, {"name": "content", "value": "${resp.xpath(&#39;//div[@id=\"chaptercontent\"]/text()&#39;)}"}]
        }
    }],
    "lineList": [{"source": 0, "target": 1, "sourceType": "Rectangle", "targetType": "Dot"}, {
        "source": 1,
        "target": 2,
        "sourceType": "Rectangle",
        "targetType": "Dot"
    }, {"source": 2, "target": 3, "sourceType": "Rectangle", "targetType": "Dot"}, {
        "source": 3,
        "target": 4,
        "sourceType": "Rectangle",
        "targetType": "Dot"
    }, {"source": 4, "target": 5, "sourceType": "Rectangle", "targetType": "Dot"}]
}