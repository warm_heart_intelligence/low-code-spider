package org.spider.spiderweb.websocket;

import lombok.Data;

@Data
public class WebSocketEvent {
    private String eventType;
    private String timestamp;
    private Object message;

    public static final String OUTPUT="output";
    public static final String LOG="log";

    public WebSocketEvent(String eventType,Object message){
        this(eventType,null,message);
    }

    public WebSocketEvent(String eventType,String timestamp,Object message){
        this.eventType=eventType;
        this.timestamp=timestamp;
        this.message=message;
    }

    public static WebSocketEvent output(Object message){
        return new WebSocketEvent(OUTPUT,message);
    }

    public static WebSocketEvent log(Object message){
        return new WebSocketEvent(LOG,message);
    }

    public static WebSocketEvent log(String timestamp,Object message){
        return new WebSocketEvent(LOG,timestamp,message);
    }

}
