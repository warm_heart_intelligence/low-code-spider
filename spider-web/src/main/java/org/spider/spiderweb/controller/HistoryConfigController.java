package org.spider.spiderweb.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.spider.api.domain.model.HistoryConfig;
import org.spider.api.domain.utilDomain.Result;
import org.spider.core.service.HistoryConfigService;
import org.spider.core.service.SpiderFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("/historyconfig")
public class HistoryConfigController {
    @Resource
    private HistoryConfigService historyConfigService;
    @Resource
    private SpiderFlowService spiderFlowService;

    @PutMapping("/{hid}")
    public Result updateFlowByConfig(@PathVariable Integer hid){
        HistoryConfig byId = historyConfigService.getById(hid);
        boolean updated = spiderFlowService.updateByConfig(byId.getConfig(), byId.getFlowId());
//        byId.setCreateTime(new Date());
//        historyConfigService.updateById(byId);
        return Result.getInstance(updated);
    }

    @GetMapping("/list")
    public Result getHistoryList(@RequestParam(name = "page", defaultValue = "1")    Integer page,
                                 @RequestParam(name = "limit", defaultValue = "1")   Integer limit,
                                 @RequestParam(name = "flowId", defaultValue = "")     String flowId){
        IPage<HistoryConfig> historyConfigIPage = historyConfigService.selectPages(new Page<>(page, limit), flowId);
        return Result.getInstance(historyConfigIPage);
    }

    @DeleteMapping("/{hid}")
    public Result deleteHistory(@PathVariable Integer hid){
        boolean b = historyConfigService.removeById(hid);
        return Result.getInstance(b);
    }

}
