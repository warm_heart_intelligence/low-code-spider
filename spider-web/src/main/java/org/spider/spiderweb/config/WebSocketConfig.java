package org.spider.spiderweb.config;

import org.spider.core.Spider;
import org.spider.spiderweb.websocket.EditorWebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@Configuration
public class WebSocketConfig {

    /**
     * ServerEndpointExporter类的作用是，会扫描所有的服务器端点，
     * 把带有@ServerEndpoint 注解的所有类都添加进来
     *
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }
    // 静态注入，endpoint中标注的类，无法使用autowired
    @Autowired
    public void setStaticSpider(Spider spider){
        EditorWebSocketServer.spider=spider;
    }
}
