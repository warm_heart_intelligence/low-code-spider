package org.spider.spiderweb;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication(
//        exclude= {DataSourceAutoConfiguration.class}
)
// 包不在家，要手动配置扫描
@MapperScan("org.spider.core.mapper")
// 扫描service
// 测试三个是否可以合并
@ComponentScan({"org.spider.api","org.spider.spiderweb"
        ,"org.spider.core"
})
public class SpiderWebApplication {
    private static final Logger log= LoggerFactory.getLogger(SpiderWebApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(SpiderWebApplication.class, args);
        log.info("low code spider backend开始运行.前台地址:http://localhost:5173/task/list");
        log.info("low code spider backend开始运行.接口文档:http://localhost:{}/doc.html",port);
        System.out.println(
                "       ___                                              __                                      __                          \n" +
                "      /\\_ \\                                            /\\ \\                              __    /\\ \\                         \n" +
                "      \\//\\ \\     ___   __  __  __        ___    ___    \\_\\ \\     __         ____  _____ /\\_\\   \\_\\ \\     __   _ __          \n" +
                "        \\ \\ \\   / __`\\/\\ \\/\\ \\/\\ \\      /'___\\ / __`\\  /'_` \\  /'__`\\      /',__\\/\\ '__`\\/\\ \\  /'_` \\  /'__`\\/\\`'__\\        \n" +
                "         \\_\\ \\_/\\ \\L\\ \\ \\ \\_/ \\_/ \\    /\\ \\__//\\ \\L\\ \\/\\ \\L\\ \\/\\  __/     /\\__, `\\ \\ \\L\\ \\ \\ \\/\\ \\L\\ \\/\\  __/\\ \\ \\/         \n" +
                "         /\\____\\ \\____/\\ \\___x___/'    \\ \\____\\ \\____/\\ \\___,_\\ \\____\\    \\/\\____/\\ \\ ,__/\\ \\_\\ \\___,_\\ \\____\\\\ \\_\\         \n" +
                "         \\/____/\\/___/  \\/__//__/       \\/____/\\/___/  \\/__,_ /\\/____/     \\/___/  \\ \\ \\/  \\/_/\\/__,_ /\\/____/ \\/_/         \n" +
                "                                                                                    \\ \\_\\                                   \n" +
                "                                                                                     \\/_/                                   \n" +
                "            __                           ____     _____   _____                                                             \n" +
                "           /\\ \\                         /\\  _`\\  /\\___ \\ /\\___ \\                                                            \n" +
                "           \\ \\ \\____  __  __            \\ \\ \\/\\ \\\\/__/\\ \\\\/__/\\ \\                                                           \n" +
                "            \\ \\ '__`\\/\\ \\/\\ \\            \\ \\ \\ \\ \\  _\\ \\ \\  _\\ \\ \\                                                          \n" +
                "             \\ \\ \\L\\ \\ \\ \\_\\ \\            \\ \\ \\_\\ \\/\\ \\_\\ \\/\\ \\_\\ \\                                                         \n" +
                "              \\ \\_,__/\\/`____ \\            \\ \\____/\\ \\____/\\ \\____/                                                         \n" +
                "               \\/___/  `/___/> \\            \\/___/  \\/___/  \\/___/                                                          \n" +
                "                          /\\___/                                                                                            \n" +
                "                          \\/__/                                                                                             ");
    }

    private static String port;
    @Value("${server.port}")
    private void setPort(String port){
        SpiderWebApplication.port =port;
    }
}


//        System.out.println(
//                "        .__                                    .___                          .__      .___                   \n" +
//                        "        |  |   ______  _  __    ____  ____   __| _/____      ______ ______   |__|   __| _/   ____   _______  \n" +
//                        "        |  |  /  _ \\ \\/ \\/ /  _/ ___\\/  _ \\ / __ |/ __ \\    /  ___/ \\____ \\  |  |  / __ |  _/ __ \\  \\_  __ \\ \n" +
//                        "        |  |_(  <_> )     /   \\  \\__(  <_> ) /_/ \\  ___/    \\___ \\  |  |_> > |  | / /_/ |  \\  ___/   |  | \\/ \n" +
//                        "        |____/\\____/ \\/\\_/     \\___  >____/\\____ |\\___  >  /____  > |   __/  |__| \\____ |   \\___  >  |__|    \n" +
//                        "                                   \\/           \\/    \\/        \\/  |__|               \\/       \\/           \n" +
//                        "                        ___.                 ________        ____.    ____.                                  \n" +
//                        "                        \\_ |__    ___.__.    \\______ \\      |    |   |    |                                  \n" +
//                        "                         | __ \\  <   |  |     |    |  \\     |    |   |    |                                  \n" +
//                        "                         | \\_\\ \\  \\___  |     |    `   \\/\\__|    /\\__|    |                                  \n" +
//                        "                         |___  /  / ____|    /_______  /\\________\\________|                                  \n" +
//                        "                             \\/   \\/                 \\/                                                      ");