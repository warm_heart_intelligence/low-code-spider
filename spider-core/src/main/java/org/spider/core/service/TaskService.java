package org.spider.core.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.spider.api.domain.vo.TaskVo;
import org.spider.api.domain.model.Task;
import org.spider.core.mapper.TaskMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Junjie
 * @description 针对表【task】的数据库操作Service实现
 * @createDate 2024-01-16 15:10:54
 */
@Service
public class TaskService extends ServiceImpl<TaskMapper, Task>
        implements IService<Task> {

    @Resource
    private TaskMapper taskMapper;

    public IPage<TaskVo> selectPages(Page<TaskVo> page, String flowId, String name) {
        return taskMapper.selectPage(page,flowId,name);
    }

}


//    public PageDto selectTaskPages(Integer page, Integer limit, String flowId, String name) {
//        Integer offset = (page - 1) * limit;
//        List<TaskVo> data = taskMapper.selectTaskPage(flowId, name, offset, limit);
//        Integer total = taskMapper.selectTotal(flowId, name);
//        PageDto pageDto = PageDto.builder().records(data)
//                .page(page)
//                .limit(limit)
//                .total(total)
//                .build();
//        return pageDto;
//    }