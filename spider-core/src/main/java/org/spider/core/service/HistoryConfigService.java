package org.spider.core.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.spider.api.domain.model.HistoryConfig;
import org.spider.core.mapper.HistoryConfigMapper;
import org.springframework.stereotype.Service;

@Service
public class HistoryConfigService extends ServiceImpl<HistoryConfigMapper, HistoryConfig>
    implements IService<HistoryConfig> {

    public IPage<HistoryConfig> selectPages(Page<HistoryConfig> page,String flowId){
        LambdaQueryWrapper<HistoryConfig> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.isNotEmpty(flowId),HistoryConfig::getFlowId,flowId)
                .orderByDesc(HistoryConfig::getCreateTime);
        return this.page(page,wrapper);
    }
}
