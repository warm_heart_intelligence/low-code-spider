package org.spider.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.spider.api.domain.model.IpProxy;
import org.spider.core.mapper.IpProxyMapper;
import org.springframework.stereotype.Service;

@Service
public class IpProxyService extends ServiceImpl<IpProxyMapper, IpProxy> implements IService<IpProxy> {
}