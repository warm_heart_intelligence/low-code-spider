package org.spider.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.spider.api.domain.model.Notice;

/**
* @author 20384
* @description 针对表【notice(存储一个flow流程图任务对应的通知人)】的数据库操作Mapper
* @createDate 2024-04-11 14:15:46
* @Entity generator.domain.Notice
*/
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {

}




