package org.spider.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.spider.api.domain.model.IpProxy;

/**
* @author 20384
* @description 针对表【ip_proxy】的数据库操作Mapper
* @createDate 2024-04-09 16:22:29
* @Entity generator.domain.IpProxy
*/
@Mapper
public interface IpProxyMapper extends BaseMapper<IpProxy> {

}




