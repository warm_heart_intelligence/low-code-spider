package org.spider.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.spider.api.domain.vo.TaskVo;
import org.spider.api.domain.model.Task;

/**
* @author Junjie
* @description 针对表【task】的数据库操作Mapper
* @createDate 2024-01-16 15:10:54
* @Entity org.spider.api.domain.model.Task
*/
@Mapper
public interface TaskMapper extends BaseMapper<Task> {
    @Select("select count(*) from low_code_spider.task where flow_id=#{flowId}")
    Integer getAllCountByFlowId(@Param("flowId") String flowId);

    @Select("select count(end_time) from low_code_spider.task where flow_id=#{flowId}")
    Integer getFinishedCountByFlowId(@Param("flowId") String flowId);

    IPage<TaskVo> selectPage(@Param("page")Page<TaskVo> page,
                             @Param("flowId") String flowId,
                             @Param("name") String name);
}



//    List<TaskVo> selectTaskPage(@Param("flowId") String flowId,
//                                @Param("name") String name,
//                                @Param("offset") Integer offset,
//                                @Param("size") Integer size);
//    Integer selectTotal(@Param("flowId") String flowId,
//                        @Param("name") String name);
