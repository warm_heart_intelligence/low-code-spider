package org.spider.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.spider.api.domain.vo.DatasourceVo;
import org.spider.api.domain.model.DataSourceModel;

import java.util.List;

/**
* @createDate 2024-02-07 19:32:54
* @Entity org.spider.api.domain.model.Datasource
*/
@Mapper
public interface DatasourceMapper extends BaseMapper<DataSourceModel> {

    @Select("select  id,name from datasource")
    List<DatasourceVo> listAll();
}




