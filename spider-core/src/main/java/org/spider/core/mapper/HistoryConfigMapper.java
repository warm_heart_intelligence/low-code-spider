package org.spider.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.spider.api.domain.model.HistoryConfig;

@Mapper
public interface HistoryConfigMapper extends BaseMapper<HistoryConfig> {
}
