package org.spider.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.spider.api.domain.model.SpiderFlow;
import org.spider.api.domain.vo.SpiderFlowVo;

import java.util.Date;

/**
* @author Junjie
* @description 针对表【flow】的数据库操作Mapper
* @createDate 2024-01-16 15:10:39
* @Entity org.spider.api.domain.model.Flow
*/
@Mapper
public interface FlowMapper extends BaseMapper<SpiderFlow> {
    @Select("select * from flow where id=#{id}")
    SpiderFlow selectById(@Param("id") String id);

    @Update("update flow set last_execute_time=#{now} where id=#{id}")
    boolean updateLastExecuteTimeById(@Param("now")Date now,@Param("id")String id);

    @Update("update flow set config=#{config} where id=#{id}")
    boolean updateConfigById(@Param("config") String config,@Param("id") String id);

    @Update("update flow set cron_enable=#{cron_enable} where id=#{id}")
    boolean updateCronStatus(@Param("id") String id,@Param("cron_enable") String cron_enable);

    @Update("update flow set cron=#{cron} where id=#{id}")
    boolean updateCron(@Param("id") String id,@Param("cron") String cron);

    IPage<SpiderFlowVo> selectPage(@Param("page")Page<SpiderFlowVo> page,
                                   @Param("name") String name
                                   );
}




