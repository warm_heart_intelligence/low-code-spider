package org.spider.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spider.api.expression.interfaces.ExpressionEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created on 2020-03-11
 *
 * @author Octopus
 */
@Component
public class ExpressionUtils {

    private static Logger logger = LoggerFactory.getLogger(ExpressionUtils.class);

    /**
     * 选择器
     */
    private static ExpressionEngine engine;

    @Autowired
    private ExpressionUtils(ExpressionEngine engine) {
        ExpressionUtils.engine = engine;
    }

    public static Object execute(String expression, Map<String, Object> variables) {
        return engine.execute(expression,variables);
    }
}
