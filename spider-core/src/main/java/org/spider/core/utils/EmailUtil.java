package org.spider.core.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class EmailUtil {
    private static boolean debug=false;
    private static Logger log = LoggerFactory.getLogger(EmailUtil.class);

    /**
     * @param from          发送方
     * @param tos            接收方
     * @param subject       主题
     * @param messageText   内容
     * @param password      授权码
     */
    public static void sendMails(String from,List<String> tos, String password,
                         String subject,String messageText) throws javax.mail.MessagingException {
        if(debug) {
            log.info("from:" + from);
            log.info("to:" + tos);
            log.info("password:" + password);
            log.info("subject:" + subject);
        }
        //生成STMP的主机名称
        int n = from.indexOf('@');
        int m = from.length();
        String mailServer = "smtp."+from.substring(n+1,m);
        //建立邮件回话
        Properties pro = new Properties();
        pro.put("mail.smtp.host",mailServer);
        pro.put("mail.smtp.auth","true");
        Session sess = Session.getInstance(pro);
        //可以查看日志
        sess.setDebug(debug);
        //新建一个消息对象
        MimeMessage message = new MimeMessage(sess);
        //设置发件人
        InternetAddress from_mail = new InternetAddress(from);
        message.setFrom(from_mail);
        //设置收件人
        Address[] addresses = new Address[tos.size()];
        for (int i = 0; i < tos.size(); i++) {
            addresses[i] = new InternetAddress(tos.get(i));
        }
        message.setRecipients(Message.RecipientType.TO,addresses);
        //设置主题
        message.setSubject(subject);
        //设置内容
        message.setText(messageText);
        //设置发送时间
        message.setSentDate(new Date());  //就今天
        Transport transport = sess.getTransport("smtp");
        transport.connect(mailServer,from,password);
        transport.sendMessage(message,message.getAllRecipients());
        transport.close();
        log.info("=============================================================");
        for(String to:tos)
            log.info("成功向用户:"+to+"发送主题为:"+subject+"的邮箱");
        log.info("时间为:"+new Date());
        log.info("=============================================================");
    }
    public static void sendMail(String from,String to, String password,
                         String subject,String messageText) throws  javax.mail.MessagingException {
        List<String> tos = new ArrayList();
        tos.add(to);
        sendMails(from,tos,password,subject,messageText);
    }

    //测试用例，邮箱存储在resources中
    public static void main(String[] args) throws javax.mail.MessagingException {
        sendMail("2038482714@qq.com","2038482714@qq.com","udrgjyrazvlgebib","测试是否能发送邮件","内容部分");
    }
}
