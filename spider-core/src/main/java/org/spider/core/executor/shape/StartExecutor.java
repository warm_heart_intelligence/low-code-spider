package org.spider.core.executor.shape;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spider.api.context.SpiderContext;
import org.spider.api.domain.utilDomain.SpiderNode;
import org.spider.api.executor.ShapeExecutor;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 荡来的,貌似没用,只是一个执行的开始需要一个root
 */
@Component
public class StartExecutor implements ShapeExecutor {
    private static Logger log= LoggerFactory.getLogger(StartExecutor.class);
    @Override
    public String supportShape() {
        return "start";
    }

    @Override
    public void execute(SpiderNode node, SpiderContext context, Map<String, Object> variables) {
        log.debug(this.getClass().getName()+"执行了");
    }

    @Override
    public boolean isThread() {
        return false;
    }
}
