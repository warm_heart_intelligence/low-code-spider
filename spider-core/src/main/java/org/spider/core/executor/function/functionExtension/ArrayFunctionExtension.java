package org.spider.core.executor.function.functionExtension;

import org.spider.api.annotation.Comment;
import org.spider.api.annotation.Example;
import org.spider.api.executor.FunctionExtension;
import org.springframework.stereotype.Component;

@Component
public class ArrayFunctionExtension implements FunctionExtension {
	
	@Override
	public Class<?> support() {
		return Object[].class;
	}
	
	@Comment("获取数组的长度")
	@Example("${arrayVar.size()}")
	public static int size(Object[] objs){
		return objs.length;
	}
}
