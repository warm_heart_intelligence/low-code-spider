package org.spider.core.executor.function.functionBasic;


import org.spider.api.annotation.Comment;
import org.spider.api.annotation.Example;
import org.spider.api.executor.FunctionExecutor;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * String 工具类 防止NPE 
 * @author Administrator
 *
 */
@Component
@Comment("string常用方法")
public class StringFunctionExecutor implements FunctionExecutor {
	
	@Override
	public String getFunctionPrefix() {
		return "string";
	}

	@Comment("截取字符串方法")
	@Example("${string.substring(str,5)}")
	public static String substring(String content, int beginIndex) {
		return content != null ? content.substring(beginIndex) : null;
	}

	@Comment("截取字符串方法")
	@Example("${string.substring(str,0,str.length() - 1)}")
	public static String substring(String content, int beginIndex, int endIndex) {
		return content != null ? content.substring(beginIndex, endIndex) : null;
	}

	@Comment("将字符串转为Integer")
	@Example("${string.toInt(value,defaultValue)}")
	public static Integer toInt(String value,Integer defaultValue){
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	@Comment("判断字符串是否为空或为null")
	@Example("${string.empty(value)}")
	public static Boolean empty(String value){
		return value==null || "".equals(value.trim());
	}
	
	@Comment("字符串替换")
	@Example("${string.replace(content,source,target)}")
	public static String replace(String content,String source,String target){
		return content != null ? content.replace(source, target): null;
	}

	@Comment("去除字符串两边的空格")
	@Example("${string.trim(content)}")
	public static String trim(String content){
		return content != null ? content.trim() : null;
	}

	@Comment("生成UUID")
	@Example("${string.uuid()}")
	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}

}
