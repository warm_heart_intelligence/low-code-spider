package org.spider.core.executor.function.functionExtension;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.spider.api.annotation.Comment;
import org.spider.api.annotation.Example;
import org.spider.api.executor.FunctionExtension;
import org.spider.core.utils.ExtractUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ElementsFunctionExtension implements FunctionExtension{

	@Override
	public Class<?> support() {
		return Elements.class;
	}
	
	@Comment("根据xpath提取内容")
	@Example("${elementsVar.xpath('//title/text()')}")
	public static String xpath(Elements elements,String xpath){
		return ExtractUtils.getValueByXPath(elements, xpath);
	}
	
	@Comment("根据xpath提取内容")
	@Example("${elementsVar.xpaths('//h2/text()')}")
	public static List<String> xpaths(Elements elements,String xpath){
		return ExtractUtils.getValuesByXPath(elements, xpath);
	}

	@Comment("返回所有attr")
	@Example("${elementsVar.attrs('href')}")
	public static List<String> attrs(Elements elements,String key){
		List<String> list = new ArrayList<>(elements.size());
		for (Element element : elements) {
			list.add(element.attr(key));
		}
		return list;
	}

	@Comment("返回所有text")
	@Example("${elementsVar.texts()}")
	public static List<String> texts(Elements elements){
		List<String> list = new ArrayList<>(elements.size());
		for (Element element : elements) {
			list.add(element.text());
		}
		return list;
	}
}
