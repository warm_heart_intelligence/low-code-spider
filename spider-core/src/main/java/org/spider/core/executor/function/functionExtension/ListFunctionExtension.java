package org.spider.core.executor.function.functionExtension;

import org.apache.commons.lang3.StringUtils;
import org.spider.api.annotation.Comment;
import org.spider.api.annotation.Example;
import org.spider.api.executor.FunctionExtension;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ListFunctionExtension implements FunctionExtension{

	@Override
	public Class<?> support() {
		return List.class;
	}
	
	@Comment("获取list的长度")
	@Example("${listVar.length()}")
	public static int length(List<?> list){
		return list.size();
	}

	@Comment("将list拼接起来")
	@Example("${listVar.join()}")
	public static String join(List<?> list){
		return StringUtils.join(list.toArray());
	}

	@Comment("将list用separator拼接起来")
	@Example("${listVar.join('-')}")
	public static String join(List<?> list,String separator){
		if(list.size() == 1){
			return list.get(0).toString();
		}else{
			return StringUtils.join(list.toArray(),separator);
		}
	}
}
