package org.spider.core.executor.function.functionExtension;

import org.spider.api.annotation.Comment;
import org.spider.api.annotation.Example;
import org.spider.api.executor.FunctionExtension;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ObjectFunctionExtension implements FunctionExtension{
	
	@Override
	public Class<?> support() {
		return Object.class;
	}
	
	@Comment("将对象转为string类型")
	@Example("${objVar.string()}")
	public static String string(Object obj){
		if (obj instanceof String) {
			return (String) obj;
		}
		return Objects.toString(obj);
	}
}
