package org.spider.core.executor.function.functionExtension;

import org.jsoup.nodes.Element;
import org.spider.api.annotation.Comment;
import org.spider.api.annotation.Example;
import org.spider.api.annotation.Return;
import org.spider.api.executor.FunctionExtension;
import org.spider.core.utils.ExtractUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ElementFunctionExtension implements FunctionExtension{

	@Override
	public Class<?> support() {
		return Element.class;
	}
	
	@Comment("根据xpath提取内容")
	@Example("${elementVar.xpath('//title/text()')}")
	@Return({Element.class,String.class})
	public static String xpath(Element element,String xpath){
		return ExtractUtils.getValueByXPath(element, xpath);
	}

	@Comment("根据xpath提取内容")
	@Example("${elementVar.xpaths('//h2/text()')}")
	@Return({Element.class,String.class})
	public static List<String> xpaths(Element element,String xpath){
		return ExtractUtils.getValuesByXPath(element, xpath);
	}

	@Comment("根据正则表达式提取内容")
	@Example("${elementVar.regx('<title>(.*?)</title>')}")
	public static String regx(Element element,String regx){
		return ExtractUtils.getFirstMatcher(element.html(), regx, true);
	}
}
